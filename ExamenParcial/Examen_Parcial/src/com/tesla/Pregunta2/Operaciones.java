package com.tesla.Pregunta2;

import java.util.ArrayList;
import java.util.List;

import com.tesla.Pregunta2.Exepciones.Vacio;

public class Operaciones {
    List<Postulantes> candidatos = new ArrayList<>();
    public void verificar(List<Postulantes> candidatos) throws Vacio{
        if(candidatos.size()<=0){
            throw new Vacio("La lista esta vacia.")
        }
        for(Postulantes postulante: candidatos){
            if(postulante.getEdad() <18 ){
                candidatos.remove(postulante);
            }
        }
    }
}
