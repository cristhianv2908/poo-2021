package com.tesla.Pregunta2;

public class Postulantes {
    private int dni;
    private String nombre;
    private String apellidos;
    private String nivelEducativo;
    private String direccion;
    private int edad;
    private int telefono;
    private int notaEvaluacion=0;
    public String getApellidos() {
        return apellidos;
    }
    public String getDireccion() {
        return direccion;
    }
    public int getDni() {
        return dni;
    }
    public int getEdad() {
        return edad;
    }
    public String getNivelEducativo() {
        return nivelEducativo;
    }
    public String getNombre() {
        return nombre;
    }
    public int getNotaEvaluacion() {
        return notaEvaluacion;
    }
    public int getTelefono() {
        return telefono;
    }
    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
    public void setDni(int dni) {
        this.dni = dni;
    }
    public void setEdad(int edad) {
        this.edad = edad;
    }
    public void setNivelEducativo(String nivelEducativo) {
        this.nivelEducativo = nivelEducativo;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public void setNotaEvaluacion(int notaEvaluacion) {
        this.notaEvaluacion = notaEvaluacion;
    }
    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }
}
