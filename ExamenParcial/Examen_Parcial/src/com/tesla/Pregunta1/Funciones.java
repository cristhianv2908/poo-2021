package com.tesla.Pregunta1;

import java.util.ArrayList;
import java.util.List;

public class Funciones {
    public List<Libro> buscarLibro(List<Libro> lista, String titulo) throws ExepcionNoLibro{
        List<Libro> libroBuscado = new ArrayList<>();
        for(Libro libro: lista){
            if(libro.getTitulo().contains(titulo)){
                libroBuscado.add(libro);
            }
        }
        return libroBuscado;
    }
    public List<Libro> buscarLibroAutor(List<Libro> lista, String nombreAutor) throws ExepcionNoLibro{
        List<Libro> libroBuscado = new ArrayList<>();
        for(Libro libro: lista){
            if(libro.getNombreAutor().contains(nombreAutor)){
                libroBuscado.add(libro);
            }
        }
        return libroBuscado;
    }
    public List<Libro> buscarLibro(List<Libro> lista, int codigo){
        List<Libro> libroBuscado = new ArrayList<>();
        for(Libro libro: lista){
            if( libro.getCodigo() == codigo ){
                libroBuscado.add(libro);
            }
        }
        return libroBuscado;
    }
}
