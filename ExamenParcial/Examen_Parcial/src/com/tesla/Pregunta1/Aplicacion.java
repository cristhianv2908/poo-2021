package com.tesla.Pregunta1;

import java.util.ArrayList;
import java.util.List;

public class Aplicacion {
    public static void main(String[] args) throws ExepcionNoLibro {
        Libro seniorDelosAnillos = new Libro();
        seniorDelosAnillos.setCodigo(1);
        seniorDelosAnillos.setNombreAutor("Tolkien");
        seniorDelosAnillos.setTitulo("El señor de los anillos");
        List<Libro> biblioteca = new ArrayList<>();
        biblioteca.add(seniorDelosAnillos);
        Libro brevesRespuestasAlasGrandesPreguntas = new Libro();
        brevesRespuestasAlasGrandesPreguntas.setCodigo(2);
        brevesRespuestasAlasGrandesPreguntas.setNombreAutor("Hawking");
        brevesRespuestasAlasGrandesPreguntas.setTitulo("Breves Respuestas a las grandes preguntas");
        Funciones buscador = new Funciones();
        List<Libro> buscado = new ArrayList<>();
        buscado = buscador.buscarLibro(biblioteca, 1);
        System.out.println("Se ha encontrado el libro: "+buscado.get(0).getTitulo());
        
    }
}