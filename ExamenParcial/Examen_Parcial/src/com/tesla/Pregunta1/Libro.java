package com.tesla.Pregunta1;

import java.util.List;

public class Libro {
    private String titulo;
    private String nombreAutor;
    private int codigo;
    private String descripcion;
    private String editorial;
    private int anioPublicacion;
    private List<String> capitulos;
    private String resumen;

    /*public Libro(String titulo,string nombreAutor, int codigo){
        this.titulo = titulo;
        this.nombreAutor= nombreAutor;
        this.codigo=codigo;
    }*/

    public String getTitulo() {
        return titulo;
    }
    public String getNombreAutor() {
        return nombreAutor;
    }
    public int getCodigo() {
        return codigo;
    }
    public int getAnioPublicacion() {
        return anioPublicacion;
    }
    public List<String> getCapitulos() {
        return capitulos;
    }
    public String getDescripcion() {
        return descripcion;
    }
    public String getEditorial() {
        return editorial;
    }
    public String getResumen() {
        return resumen;
    }
    public void setAnioPublicacion(int anioPublicacion) {
        this.anioPublicacion = anioPublicacion;
    }
    public void setCapitulos(List<String> capitulos) {
        this.capitulos = capitulos;
    }
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    public void setEditorial(String editorial) {
        this.editorial = editorial;
    }
    public void setResumen(String resumen) {
        this.resumen = resumen;
    }
    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }
    public void setNombreAutor(String nombreAutor) {
        this.nombreAutor = nombreAutor;
    }
    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }
}
