import com.example.demo.dto.Usuario;

public class daoImpl implements Dao{
    @Autowired
    private JdbcTemplate jdbcTemplate;
    private Connection conexion;
    private void crearConexion(){
        try {
            conexion = jdbcTemplate.getDataSource().getConnection();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
    private void cerrarConexion(){
        try {
            conexion.commit();
            conexion.close();
            conexion = null;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

    }
    @Override
    public List<Usuario> obtenerUsuario() {
        List<Usuario> lista = new ArrayList<>();
        StringBuilder sb = new StringBuilder();
        sb.append(" select nombres, apellidos, correo").
            append(" from usuario").
            append(" where nombres = ?");
        crearConexion();
        try {
            PreparedStatement sentencia = conexion.prepareStatement(sb.toString());
            sentencia.setString(1, nombres.getNombres());
            ResultSet resultado = sentencia.executeQuery();
            while(resultado.next()){
                Usuario usuario = new Usuario();
                usuario.setNombres(resultado.getString("nombres"));
                usuario.setCodigo_empleado(resultado.getString("id_usuario"));
                lista.add(usuario);
            }
            resultado.close();
            sentencia.close();
            cerrarConexion();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return lista;

    }
}
