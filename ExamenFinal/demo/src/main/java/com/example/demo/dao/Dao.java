public interface Dao {
    public List<Empleado> obtenerUsuario();
    public Actividad actualizarDatos();
}
