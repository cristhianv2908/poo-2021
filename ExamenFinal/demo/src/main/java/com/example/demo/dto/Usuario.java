public class Usuario {
    private int id_usuario;
    private String nombres;
    private String apellidos;
    private String correo;
    private String administrador;
    private String contrasenia;
    public String getAdministrador() {
        return administrador;
    }
    public void setAdministrador(String administrador) {
        this.administrador = administrador;
    }public String getApellidos() {
        return apellidos;
    }public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }public String getContrasenia() {
        return contrasenia;
    }public void setContrasenia(String contrasenia) {
        this.contrasenia = contrasenia;
    }public String getCorreo() {
        return correo;
    }public void setCorreo(String correo) {
        this.correo = correo;
    }public int getId_usuario() {
        return id_usuario;
    }public void setId_usuario(int id_usuario) {
        this.id_usuario = id_usuario;
    }public String getNombres() {
        return nombres;
    }public void setNombres(String nombres) {
        this.nombres = nombres;
    }
}
