package com.tesla.Pregunta2;
import java.util.Scanner;

public class alplicacion {
    public static void main(String[] args) {
        int num_1;
        double num_2;
        Scanner entrada = new Scanner(System.in);
        System.out.println("Ingrese el numero de lados: ");
        num_1=entrada.nextInt();
        if(num_1<7){
            System.out.println("Ingrese el valor del lado: ");
            num_2=entrada.nextDouble();
            Poligono poligono = new Poligono(num_1,num_2);
            System.out.println("Perimetro: "+ poligono.getPerimetro());
            System.out.println("Area: "+ poligono.getArea());
        }
        else{
            System.out.println("El numero de lados excede la capacidad de este programa.");
        }
    }
}
