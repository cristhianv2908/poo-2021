package com.tesla.Pregunta2;

import java.util.Set;

public class Poligono {
    private int num_lados;
    private double lado;
    private double area;
    private double perimetro;
    public double getArea() {
        return area;
    }
    public void setArea(double area) {
        this.area = area;
    }
    public double getLado() {
        return lado;
    }
    public void setLado(double lado) {
        this.lado = lado;
    }
    public int getNum_lados() {
        return num_lados;
    }
    public void setNum_lados(int num_lados) {
        this.num_lados = num_lados;
    }
    public double getPerimetro() {
        return perimetro;
    }
    public void setPerimetro(double perimetro) {
        this.perimetro = perimetro;
    }
    public Poligono (int num_lados, double lado){
        this.num_lados=num_lados;
        this.lado=lado;
        calcular_perimetro();
        calcular_area();
    }
    public void calcular_perimetro(){
        this.perimetro=this.num_lados*this.lado;
    }
    public void calcular_area(){
        double apotema=0;
        apotema = this.lado / (2*Math.tan((360/(2*this.num_lados)*Math.PI /180)));
        this.area=this.perimetro*apotema /2 ;
    }
}
