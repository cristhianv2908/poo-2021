package com.practica3.demo.servicio;

import java.util.List;

import com.practica3.demo.dto.Actividad;
import com.practica3.demo.dto.Asignacion;
import com.practica3.demo.dto.Departamento;
import com.practica3.demo.dto.Empleado;
public interface Servicio {
    public List<Empleado> obtenerEmpleado(Departamento departamento);
    public Actividad agregarActividad(Actividad actividad);
    public Asignacion obtenerAsignaciones(Empleado empleado);
    public void actualizarUbicacion(Departamento departamento);
}
