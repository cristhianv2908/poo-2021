package com.practica3.demo.dto;
import lombok.Data;

@Data
public class Actividad {
    private Integer prioridad;
    private Integer id_actividad;
    private String nombre;
}
