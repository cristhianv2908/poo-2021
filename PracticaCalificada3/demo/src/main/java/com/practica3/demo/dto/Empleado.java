package com.practica3.demo.dto;
import lombok.Data;

@Data
public class Empleado {
    private String nombres;
    private String apellidos;
    private String codigo_empleado;
    private String codigo_departamento;
}
