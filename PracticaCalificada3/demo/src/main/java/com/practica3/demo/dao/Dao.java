package com.practica3.demo.dao;

import java.util.List;

import com.practica3.demo.dto.*;

public interface Dao {
    public List<Empleado> obtenerEmpleado(Departamento departamento);
    public Actividad agregarActividad(Actividad actividad);
    public Asignacion obtenerAsignaciones(Empleado empleado);
    public void actualizarUbicacion(Departamento departamento);
}
