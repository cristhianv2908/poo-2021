package com.practica3.demo.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import com.practica3.demo.dto.Actividad;
import com.practica3.demo.dto.Asignacion;
import com.practica3.demo.dto.Departamento;
import com.practica3.demo.dto.Empleado;
@Repository

public class DaoImpl implements Dao {
    @Autowired
    private JdbcTemplate jdbcTemplate;
    private Connection conexion;
    private void crearConexion(){
        try {
            conexion = jdbcTemplate.getDataSource().getConnection();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
    private void cerrarConexion(){
        try {
            conexion.commit();
            conexion.close();
            conexion = null;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

    }
    @Override
    public List<Empleado> obtenerEmpleado(Departamento departamento) {
        List<Empleado> lista = new ArrayList<>();
        StringBuilder sb = new StringBuilder();
        sb.append(" select nombres, apellidos, codigo_departamento").
            append(" from empleado").
            append(" where codigo_departamento = ?");
        crearConexion();
        try {
            PreparedStatement sentencia = conexion.prepareStatement(sb.toString());
            sentencia.setString(1, departamento.getCodigo_departamento());
            ResultSet resultado = sentencia.executeQuery();
            while(resultado.next()){
                Empleado empleado = new Empleado();
                empleado.setNombres(resultado.getString("nombres"));
                empleado.setCodigo_empleado(resultado.getString("codigo_empleado"));
                lista.add(empleado);
            }
            resultado.close();
            sentencia.close();
            cerrarConexion();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return lista;

    }
    @Override
    public Actividad agregarActividad(Actividad actividad) {
        // TODO Auto-generated method stub
        return null;
    }
    @Override
    public Asignacion obtenerAsignaciones(Empleado empleado) {
        // TODO Auto-generated method stub
        return null;
    }
    @Override
    public void actualizarUbicacion(Departamento departamento) {
        // TODO Auto-generated method stub
        
    }

}
