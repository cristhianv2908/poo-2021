package com.practica3.demo.dto;
import lombok.Data;

@Data
public class Asignacion {
    private String codigo_empleado;
    private Integer id_asignacion;
    private Integer id_actividad;
    private Integer presupuesto;
}
