package com.practica3.demo.dto;
import lombok.Data;

@Data


public class Departamento {
    private String nombres;
    private String ubicacion;
    private String codigo_departamento;
}
