package com.tesla.Pregunta1;


public class Monedas {
    private double valorPorDolar;
    private String nombre;
    public String getNombre() {
        return nombre;
    }
    public double getValorPorDolar() {
        return valorPorDolar;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public void setValorPorDolar(double valorPorDolar) {
        this.valorPorDolar = valorPorDolar;
    }
    public Monedas(double valorPorDolar, String nombre){
        this.valorPorDolar = valorPorDolar;
        this.nombre=nombre;
    }
}
