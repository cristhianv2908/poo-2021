package com.tesla.Pregunta1;

public class Aplicacion {
    public static void main(String[] args) {
        Monedas Yen = new Monedas(109.25, "Yen");
        Monedas Soles = new Monedas(3.71, "Soles");
        double cantidad_moneda1 = 29.08;
        double cantidad_moneda2 = Conversion.convertir(cantidad_moneda1, Yen, Soles);
        System.out.println("La cantidad "+cantidad_moneda1+" en yenes equivale a "+cantidad_moneda2+" en soles");
    }
}
