package com.tesla.Pregunta1;

public class Conversion {
    double convertir(double cantidad, Monedas moneda, Monedas moneda_nueva){
        double cantidad_dolares;
        double cantidad_nueva;
        cantidad_dolares = cantidad/moneda.getValorPorDolar();
        cantidad_nueva = cantidad_dolares * moneda_nueva.getValorPorDolar();
        return cantidad_nueva;
    }
}