package com.testmaven;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Hello world!
 *
 */
public class App 
{
    private final static String urlBD = "jdbc:mariadb://127.0.0.1:3306/poo";
    private final static String usuarioBD = "root";
    private final static String credencialBD = "v4ler1o2908";
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
        
        try {
            Class.forName("org.mariadb.jdbc.Driver");
            Connection conexion = DriverManager.getConnection( urlBD, usuarioBD, credencialBD);
            Statement sentencia = conexion.createStatement();
            String sql =    " SELECT codigo_producto codigo, nombre nombreProducto "+
                            " FROM producto ";
            ResultSet resultado = sentencia.executeQuery(sql);
            while(resultado.next()){
                System.out.println( resultado.getString("nombreProducto"));
            }
            resultado.close();
            sentencia.close();
            conexion.close();


        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        

    }
}
